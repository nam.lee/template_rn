# Install Eslint and Prettier in extensions VsCode

# Fonts asset linking instruction

- run npx react-native link
- if use with react-native-vector-icons, remove fonts of vector-icons in copy bundle resources
- Define font family names in index.d.ts in components/Typography for auto suggestion.

# Structure explain

- src: contain javascript source code
  - apis:
    - Token Management for refreshing token and provide token
    - BE: contains endpoint and services path
  - components: only contain reuseable components and must have d.ts to support suggestion
  - hooks: only contain reusable hooks
  - locales: contains translations files
  - routes: defines app route here
  - screens: contains screens
  - stores: global store for apps
  - utils: app ultilities

### Notice

- Each screen, feature should do one things at a time to make it's consistent through time and easy to change, fix bugs, or modify.
- DO NOT MAKE a screen or feature, or function do two different things.
