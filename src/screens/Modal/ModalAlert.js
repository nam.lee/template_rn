import React from 'react';
import Box from '@src/components/Box';
import Typography from '@src/components/Typography';
import TouchableBox from '@src/components/TouchableBox';
import colors from '@src/utils/colors';

const ModalAlert = (navigation, router) => {
  // const { title } = router.params;

  return (
    <Box justify="center" align="center" flex={1} margin={[0, 16]}>
      <Box background={colors.white} borderRadius={4}>
        {/* <Typography>{title || 'Thông báo'}</Typography> */}
      </Box>
      <TouchableBox>
        <Typography>OK</Typography>
      </TouchableBox>
    </Box>
  );
};

export default ModalAlert;
