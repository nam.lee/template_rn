import React, { useCallback } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { useNavigation } from '@react-navigation/native';
import Home from '@src/screens/Home';
import Notification from '@src/screens/Notification';
import TouchableBox from '@src/components/TouchableBox';
import ImageIcon from '@src/components/ImageIcon';
import ModalAlert from '@src/screens/Modal/ModalAlert';
const Stack = createStackNavigator();

const HeaderLeft = () => {
  const navigation = useNavigation();
  const goBack = useCallback(() => {
    navigation.pop();
  }, [navigation]);

  return (
    <TouchableBox square={40} onPress={goBack} justify="center" align="center">
      <ImageIcon name="backArrow" square={24} />
    </TouchableBox>
  );
};

const HeaderDrawer = () => {
  const navigation = useNavigation();
  const toggleDrawer = useCallback(() => {
    navigation.openDrawer();
  }, [navigation]);

  return (
    <TouchableBox
      square={40}
      onPress={toggleDrawer}
      justify="center"
      align="center">
      <ImageIcon name="menu" square={24} />
    </TouchableBox>
  );
};

const MainStack = () => {
  const headerLeft = useCallback(() => <HeaderLeft />, []);
  const headerDrawer = useCallback(() => <HeaderDrawer />, []);

  return (
    <Stack.Navigator initialRouteName="Home">
      <Stack.Screen
        name="Home"
        component={Home}
        options={{
          title: 'Home',
          headerLeft: headerDrawer,
        }}
      />
      <Stack.Screen
        name="Notification"
        component={Notification}
        options={{ title: 'Notification', headerLeft }}
      />
    </Stack.Navigator>
  );
};

const RootStack = () => {
  return (
    <Stack.Navigator mode="modal" initialRouteName="MainStack">
      <Stack.Screen
        name="MainStack"
        component={MainStack}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="ModalAlert"
        component={ModalAlert}
        options={{
          // headerShown: false,
          cardStyle: { backgroundColor: 'rgba(0,0,0,0.6)' },
          cardStyleInterpolator: ({ current: { progress } }) => {
            return {
              cardStyle: {
                opacity: progress.interpolate({
                  inputRange: [0, 0.5, 0.9, 1],
                  outputRange: [0, 0.25, 0.7, 1],
                }),
              },
            };
          },
        }}
      />
    </Stack.Navigator>
  );
};

export default RootStack;
