import React from 'react';
import Box from '../Box';
import Map from '@src/components/LocationPicker/Map';

const LocationPicker = (props) => {
  const { geometry } = props?.route?.params;
  return (
    <Box flex={1}>
      <Map
        // onChangeLocation={this.onChangeLocation}
        currentLocation={geometry}
        editable={false}
      />
    </Box>
  );
};

export default LocationPicker;
