import React, { useCallback, useState } from 'react';
import Box from '../Box';
import { Modal, StyleSheet, TouchableWithoutFeedback } from 'react-native';
import TouchableBox from '../TouchableBox';
import colors from '@src/utils/colors';
import ImageIcon from '../ImageIcon';
import Typography from '../Typography';
import dayjs from 'dayjs';
import DateTimePicker from '@react-native-community/datetimepicker';
import ZTButton from '../ZTButton';
import i18n from '@src/locales';

const DateTime = ({ input }) => {
  const [date, setDate] = useState(
    input.value ? new Date(input.value.valueOf()) : null,
  );
  const [dateShow, setDateShow] = useState(null);
  const [isModalDate, setModalDate] = useState(false);

  const onChange = useCallback(
    (event, selectedDate) => {
      const currentDate = selectedDate || date;
      setDate(currentDate);
      input.onChange(date);
    },
    [date, input],
  );

  const toggleModalAsset = useCallback(() => {
    setModalDate(!isModalDate);
  }, [isModalDate]);

  const showDatepicker = useCallback(() => {
    toggleModalAsset();
  }, [toggleModalAsset]);

  const onSelected = useCallback(() => {
    setDateShow(date);
    if (!input.value || input.value === '') {
      setDate(new Date());
    }
    toggleModalAsset();
  }, [input, toggleModalAsset, date]);

  return (
    <Box>
      <TouchableBox
        onPress={showDatepicker}
        flexDirection="row"
        justify="space-between"
        style={styles.default}
      >
        <Typography style={styles.input} color={colors.ZT_normal_grey_1}>
          {dateShow === null
            ? i18n.t('select.hour')
            : dayjs(dateShow).format('h:mm A')}
        </Typography>
        <ImageIcon name="clock" square={16} style={styles.icon} />
      </TouchableBox>
      <Modal animationType="slile" transparent={true} visible={isModalDate}>
        <TouchableWithoutFeedback onPress={toggleModalAsset}>
          <Box flex={1} justify="flex-end" shadowDepth={5}>
            <Box background={colors.ZT_white}>
              <TouchableBox
                title="Hide modal"
                onPress={toggleModalAsset}
                padding={[16, 16]}
              >
                <ImageIcon name="delete" square={14} />
              </TouchableBox>
              <Box>
                <DateTimePicker
                  testID="dateTimePicker"
                  value={date ? date : new Date()}
                  mode={'time'}
                  is24Hour={true}
                  display="inline"
                  onChange={onChange}
                />
              </Box>

              <ZTButton
                type="primary"
                title={i18n.t('Chọn')}
                onPress={onSelected}
                margin={[24, 16]}
              />
            </Box>
          </Box>
        </TouchableWithoutFeedback>
      </Modal>
    </Box>
  );
};

export default DateTime;

const styles = StyleSheet.create({
  input: {
    alignSelf: 'center',
    paddingLeft: 8,
  },
  icon: {
    justifyContent: 'center',
    alignSelf: 'center',
    marginRight: 8,
  },
  default: {
    borderRadius: 4,
    paddingVertical: 4,
    borderWidth: 1,
    borderColor: colors.ZT_normal_grey_2,
    height: 32,
  },
});
