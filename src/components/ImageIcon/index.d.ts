import { ImageProps } from 'react-native';

  interface ImageIconProps extends ImageProps {
    square: number;
    circle: number;
    name: 'arrowBack';
  }
  
  export default function ImageIcon(props: ImageIconProps): {};
  