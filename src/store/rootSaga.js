import { all } from 'redux-saga/effects';
import staticResourcesWatchers from './staticResources/saga';

export default function* rootSaga() {
  yield all([
    // for watcher
    staticResourcesWatchers(),
  ]);
}
