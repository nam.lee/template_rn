import TYPES from '../types';

const initState = {};

export default (state = initState, { type, payload }) => {
  switch (type) {
    case TYPES.EXAMPLE:
      return { ...state, ...payload };
    default:
      return state;
  }
};
