import { takeLatest, all, put, call } from 'redux-saga/effects';
import { syncStaticService } from './services';
import TYPES from '../types';

function* syncStaticResources() {
  try {
    const result = yield call(syncStaticService);
    yield put({
      type: TYPES.SET_STATIC_RESOURCES,
      payload: {
        example: result ? result[0].data : [],
      },
    });
  } catch (error) {}
}

export default function* staticResourcesWatchers() {
  yield all([takeLatest(TYPES.EXAMPLE, syncStaticResources)]);
}
