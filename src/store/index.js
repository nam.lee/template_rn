import AsyncStorage from '@react-native-async-storage/async-storage';
import { applyMiddleware, createStore } from 'redux';
import { persistReducer, persistStore } from 'redux-persist';
import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import rootReducer from './rootReducer';
import rootSaga from './rootSaga';

const middlewares = [];

const sagaMiddleware = createSagaMiddleware();
middlewares.push(sagaMiddleware);

const persistConfig = {
  key: '@ZotaPMS',
  storage: AsyncStorage,
  whitelist: ['auth', 'staticResources'],
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

const createPersistedSagaStore = () => {
  const store = createStore(
    persistedReducer,
    composeWithDevTools(applyMiddleware(...middlewares)),
  );

  const persistor = persistStore(store);

  sagaMiddleware.run(rootSaga);

  return { store, persistor };
};

const ZotaPmsStore = createPersistedSagaStore();

export default ZotaPmsStore;
